var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var inProduction = (process.env.NODE_ENV === 'production');

module.exports = {
    
    entry: './application/src/js/main.js',
    
    output: {
        path: path.resolve(__dirname, './application/dist/js'),
        filename: 'complied.js'
    },
    
    module:{
        rules:[
            {
                test: /\.s[ac]ss$/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader'], 
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }
        ]
    },
    
    plugins: [
        new ExtractTextPlugin('../css/style.css')    
    ]
    
};

// Only run when building for production
if(inProduction){
    
    module.exports.plugins.push(
            
        //Uglify the JS
        new webpack.optimize.UglifyJsPlugin(),

        // Minify 
        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
        
    );
}