<?php

use duncan3dc\Laravel\BladeInstance;
use Whoops\Run;

class MY_Controller extends CI_Controller
{
    protected $blade;

    function __construct()
    {
        parent::__construct();
        
        // Load blade
        $this->blade = new BladeInstance(__DIR__ . "/../views", __DIR__ . "/../cache/views");
        
        // If enviorment is development then show whoops errors
        if (ENVIRONMENT == 'development') {
            $whoops = new Whoops\Run();
            $whoops->pushHandler(new Whoops\Handler\PrettyPageHandler());
            $whoops->register();
        }
    }
}