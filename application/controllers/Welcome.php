<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Whoops\Run;

class Welcome extends MY_Controller {
    
    public function __construct() 
    {
       parent::__construct();

    }
    
	public function index()
	{   
        $data = array('content' => 'This is the main content for this page');
        echo $this->blade->render("index", $data);
	}
}
