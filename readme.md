# Codeigniter starter set-up

This is the default codeigniter version 3.1.3 framework with some changes to it:

* It has webpack integrated for frontend builds (ES15 and SASS)
* Uses the Laravel Blade templating engine for all views
* Has the whoops package for displaying errors in development mode

## Usage

clone the repo:

`git clone https://gitlab.com/marktwigg/ci3-starter.git`

Change directory into the newly created folder and run

`npm install`

and

`composer install`

### Updating build files

You can now run the following commands:

`npm run dev`

This builds your JS and SASS files but doesn't minify them

`npm run watch`

Same as above but watches for changes and automatically updates build files

`npm build production`

Makes a final build of files to be deployed, these files are minifed.

### Include the build files

You need to add the final CSS and JS files to your porject, you can do this by placing links to the built JS and CSS in your main template file.

`<style src="application/dist/css/main.css"></style>
<script src="application/dist/js/main.js"></script>`

### Blade views

You can view the full blade documentaiton here: https://laravel.com/docs/master/blade

In short all blade files are held in the views folder. You can pass data to the views by constructing an array and passing it to the $this->blade->render() method

`echo $this->blade->render("index", $data);`

The first arugment is the blade file to load, if you have view files in subfolders then you can reference them with the period notation:

`echo $this->blade->render("subfolder.amdin.index", $data);`

To access a vairable you passed in the array to blade you simply reference it using 2 curly braces {{ }}

`{{ $name_of_array_key }}`

Ensure all your controller classes extend MY_Controller so you have access to the blade instance

`class Welcome extends MY_Controller`

You can use all of blades features such as extending templates, loops and variables.

### Whoops

The default error reporting has been overridden to use the whoops libary to display usefual errors and stack traces. You can see more informaiton about the whoops libary here: http://filp.github.io/whoops/

### SASS

This set-up uses SASS for its css pre-processor. Place all your SASS files in the /application/src/css folder and webpack will automatically pick these up and complie them for you